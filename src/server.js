import express from 'express'
import viewEngine from './config/viewEngine'
import initWebRoutes from './routes/web'
import bodyParser from 'body-parser'
require('dotenv').config()
const app = express()
const PORT = process.env.PORT
//config viewEngine
viewEngine(app)

//parse request to json
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

//init web routes
initWebRoutes(app)
app.listen(PORT || 3000, () => {
  console.log('app running port :' + PORT)
})
